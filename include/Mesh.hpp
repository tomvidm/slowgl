#pragma once

#include <vector>

#include <glm/glm.hpp>

#include "graphics/Color.hpp"
#include "math/Rect.hpp"

namespace sgl {
    using Triangle = std::array<unsigned, 3>;
    using Quad = std::array<unsigned, 4>;

    class Mesh {
    public:
        Mesh(
            const std::vector<glm::vec3>& vertices,
            const std::vector<glm::vec3>& normals,
            const std::vector<Color>& colors,
            const std::vector<unsigned>& indices
        );

        static Mesh Plane(const math::Rect<float>& rect);
        static Mesh UnitBox();

        inline std::vector<glm::vec3>& getVertices() { return vertices; }
        inline std::vector<glm::vec3>& getNormals() { return normals; }
        inline std::vector<Color>& getColors() { return colors; }
        inline std::vector<unsigned>& getIndices() { return indices; }
    private:
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<Color> colors;
        std::vector<unsigned> indices;
    };
}