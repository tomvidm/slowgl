#pragma once

#include "graphics/Renderable.hpp"

namespace sgl { namespace graphics {
    class SimplePlane
    : public Renderable {
    public:
        virtual void draw(std::shared_ptr<Camera> camera, RenderState state) const;
    private:
        int i = 0;
    };
}
}