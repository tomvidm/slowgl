#pragma once

#include "Transformable.hpp"
#include "graphics/RenderState.hpp"
#include "Camera.hpp"

namespace sgl { namespace graphics {
    class Renderable
    : public Transformable {
    public:
        virtual void draw(std::shared_ptr<Camera> camera, RenderState state) const = 0;
    };
}
}