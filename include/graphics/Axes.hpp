#pragma once

#include <glm/glm.hpp>

#include "graphics/VertexBuffer.hpp"
#include "graphics/Renderable.hpp"
#include "Mesh.hpp"

namespace sgl { namespace graphics {
    class Axes
    : public Renderable {
    public:
        Axes();

        virtual void draw(std::shared_ptr<Camera> camera, RenderState state) const;
        
    private:
        VertexBuffer vbuf;
    };
}
}