#pragma once

#include <vector>

extern "C" {
    #include <GL/glew.h>
    #include <GL/gl.h>
    #include <GL/glu.h>
}

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "debug/Logger.hpp"
#include "graphics/Renderable.hpp"
#include "graphics/Vertex.hpp"

namespace sgl { namespace graphics {
    struct Quad {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec4 color;
        glm::vec2 scale;
        glm::vec2 origin;
    };

    class QuadArray
    : public Renderable {
    public:
        QuadArray();
        size_t addQuad(const Quad& quad);


        virtual void draw(std::shared_ptr<Camera> camera, RenderState state) const;
    private:
        void bufferVertices() const;

        mutable bool toBufferData;

        GLuint vaoID;
        GLuint vertexID;
        GLuint elementsID;

        std::vector<Vertex> vertices;
        std::vector<unsigned> indices;
    };
}
}