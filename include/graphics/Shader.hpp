#pragma once

#include <string>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <memory>
#include <map>

extern "C" {
    #include <GL/glew.h>
    #include <GL/gl.h>
    #include <GL/glu.h>
}

#include <glm/glm.hpp>

#include "Camera.hpp"

namespace sgl { namespace graphics {
    class Shader {
    public:
        Shader(const std::string& pathToVert,
               const std::string& pathToFrag);
        
        void use() const;
        
        inline GLuint getProgramID() const { return shaderProgramID; }

        void setModelMatrix(const glm::mat4& mview);
        void setCamera(std::shared_ptr<Camera>& camera);

        void setUint(const char* uniformID, unsigned in);
        void setInt(const char* uniformID, int in);
        void setFloat(const char* uniformID, float in);
        void setMat3(const char* uniformID, glm::mat3& in);
        void setMat3(const char* uniformID, std::vector<glm::mat3>& invec);
        void setMat4(const char* uniformID, glm::mat4& in);
        void setMat4(const char* uniformID, std::vector<glm::mat4>& invec);
        void setVec3(const char* uniformID, glm::vec3& in);
        void setVec3(const char* uniformID, std::vector<glm::vec3>& invec);
        void setVec4(const char* uniformID, glm::vec4& in);
        void setVec4(const char* uniformID, std::vector<glm::vec4>& invec);
    private:
        GLuint shaderProgramID;
        GLuint modelMat;
        GLuint viewMat;
        GLuint projMat;
        GLuint viewProjMat;
        GLuint normalMat;

        // Cache for uniform locations
        std::map<std::string, GLuint> uniformMap;

    };

    using SharedShader = std::shared_ptr<Shader>;
}
}