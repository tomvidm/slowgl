#pragma once

#include <glm/glm.hpp>

namespace sgl {
    using Color = glm::vec4;
}