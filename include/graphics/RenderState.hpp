#pragma once

#include <glm/glm.hpp>

#include "graphics/Shader.hpp"

namespace sgl { namespace graphics {
    struct RenderState{
        RenderState() = delete;
        RenderState(const glm::mat4& transform,
                    SharedShader& shader)
        : transform(transform),
          shader(shader) {;}

        glm::mat4 transform;
        SharedShader shader;
    };
}
}