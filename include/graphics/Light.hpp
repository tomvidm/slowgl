#pragma once

#include <glm/glm.hpp>

namespace sgl { namespace graphics {
    struct Light {
        glm::vec3 position;
        glm::vec3 intensity;
    };
}
}