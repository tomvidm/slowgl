#pragma once

#include <glm/glm.hpp>

extern "C" {
    #include <GL/glew.h>
    #include <GL/gl.h>
    #include <GL/glu.h>
}

namespace sgl { namespace graphics {
    struct Vertex {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec4 color;
        glm::vec2 texCoord;
    };
}
}