#pragma once

#include <windows.h>

#include <vector>

#include <glm/glm.hpp>

extern "C" {
    #include <GL/glew.h>
    #include <GL/gl.h>
    #include <GL/glu.h>
}

#include "graphics/RenderState.hpp"
#include "graphics/Renderable.hpp"
#include "graphics/Color.hpp"
#include "graphics/Shader.hpp"
#include "Mesh.hpp"

namespace sgl { namespace graphics {
    class VertexBuffer {
    public:
        VertexBuffer();
        ~VertexBuffer();

        void assignMesh(Mesh& mesh);
        void assignVertices(std::vector<glm::vec3>& pointsArray);
        void assignNormals(std::vector<glm::vec3>& normalsArray);
        void assignColors(std::vector<Color>& colorsArray);
        void assignIndices(std::vector<unsigned>& indexArray);
        void assignParentBones(std::vector<unsigned>& parentBoneArray);
        void assignPrentBoneWeights(std::vector<float>& parentBoneWeights);

        virtual void draw(std::shared_ptr<Camera> camera, RenderState state, GLuint primitiveType = GL_TRIANGLES) const;
    private:
        GLuint vao;

        size_t numVertices;
        GLuint verticesHandle;
        glm::vec3* verticesPtr;

        size_t numNormals;
        GLuint normalsHandle;
        glm::vec3* normalsPtr;

        size_t numIndices;
        GLuint indicesHandle;
        unsigned* indicesPtr;

        size_t numColors;
        GLuint colorsHandle;
        Color* colorsPtr;

        GLuint parentBoneIDHandle;
        unsigned* parentBoneIDPtr;

        GLuint parentBoneWeightsHandle;
        float* parentBoneWeightsPtr;
    };
}
}