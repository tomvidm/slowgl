#pragma once

#include <glm/glm.hpp>

namespace sgl {
    class Camera {
    public:
        Camera();

        void setOrigin(const glm::vec3 origin);
        void setPosition(const glm::vec3 position);
        void setDirection(const glm::vec3 direction);
        void setEulerAngles(const glm::vec3 eulerAngles);

        void move(const glm::vec3 displacement);

        void lookat(const glm::vec3 target);
        
        void rotate(const glm::vec3 axis, const float deg);
        void rotateViewportX(const float deg);
        void rotateViewportY(const float deg);
        glm::vec3 getViewportX() const;
        glm::vec3 getViewportY() const;

        inline glm::vec3 getDirection() const { return m_dir; }
        
        void setFOV(const float angle);
        void setAspectRatio(const float height, const float width);
        void setAspectRatio(const float aspectRatio);
        void setNearClipPlane(const float nearZ);
        void setFarClipPlane(const float farZ);

        const glm::mat4& getProjectionMatrix() const;
        const glm::mat4& getViewMatrix() const;
        const glm::mat4& getViewProjMatrix() const;

        glm::vec3 projectPoint(const glm::vec3 point) const;
    private:
        glm::vec3 m_origin;
        glm::vec3 m_position;
        glm::vec3 m_eulerAngles;
        glm::vec3 m_dir;
        glm::vec3 m_view_x;
        glm::vec3 m_view_y;

        float fov;
        float aspect;
        float nearClippingPlane;
        float farClippingPlane;

        mutable bool projMatrixNeedsUpdate;
        mutable bool viewMatrixNeedsUpdate;
        mutable bool compositeMatrixNeedsUpdate;

        mutable glm::mat4 projMatrix;
        mutable glm::mat4 viewMatrix;
        mutable glm::mat4 composite;
    };
}