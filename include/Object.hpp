#pragma once

#include <memory>
#include <vector>

#include "Transformable.hpp"

namespace sgl {
    class Object;
    using SharedObject = std::shared_ptr<Object>;

    class Object {
    public:
        
    private:
        Object* parentObject;
        std::vector<SharedObject> childObjects;
    };
}