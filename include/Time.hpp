#pragma once

namespace sgl {
    class Time {
    public:
        Time();
        Time(const unsigned long us);
        Time(const Time& other);

        static Time microseconds(const unsigned long us);
        static Time milliseconds(const unsigned long ms);
        static Time seconds(const float s);

        inline float asSeconds() const { return static_cast<float>(m_us) / 1000000.f; }

        friend bool operator < (const Time& lhs, const Time& rhs);
        friend bool operator == (const Time& lhs, const Time& rhs);
        friend bool operator > (const Time& lhs, const Time& rhs);

        friend Time operator + (const Time& lhs, const Time& rhs);
        friend Time operator - (const Time& lhs, const Time& rhs);
    protected:
        const unsigned long m_us;
    };

    bool operator < (const Time& lhs, const Time& rhs);
    bool operator == (const Time& lhs, const Time& rhs);
    bool operator > (const Time& lhs, const Time& rhs);

    Time operator + (const Time& lhs, const Time& rhs);
    Time operator - (const Time& lhs, const Time& rhs);
}