#pragma once

namespace sgl { namespace math {
    template <typename T>
    class Clamped {
    public:
        Clamped(const T val, const T min, const T max);
        Clamped(const Clamped& other);

        const T val;
        const T min;
        const T max;
    };

    template <typename T>
    Clamped<T>::Clamped(const T val, const T min, const T max)
    : val(val), min(min), max(max) {
        if (val < min) { val = min}
        else if (val > max) { val = max; }
    }

    template <typename T>
    Clamped<T> operator + (const Clamped<T> lhs, const Clamped<T> rhs) {
        return Clamped(lhs.val + rhs.val, lhs.min, lhs.max);
    }

    template <typename T>
    Clamped<T> operator - (const Clamped<T> lhs, const Clamped<T> rhs) {
        return Clamped(lhs.val - rhs.val, lhs.min, lhs.max);
    }

    template <typename T>
    Clamped<T> operator * (const Clamped<T> lhs, const Clamped<T> rhs) {
        return Clamped(lhs.val * rhs.val, lhs.min, lhs.max);
    }

    template <typename T>
    Clamped<T> operator / (const Clamped<T> lhs, const Clamped<T> rhs) {
        return Clamped(lhs.val / rhs.val, lhs.min, lhs.max);
    }

    template <typename T>
    Clamped<T> operator + (const Clamped<T> lhs, const T rhs) {
        return Clamped(lhs.val + rhs, lhs.min, lhs.max);
    }

    template <typename T>
    Clamped<T> operator - (const Clamped<T> lhs, const T rhs) {
        return Clamped(lhs.val - rhs, lhs.min, lhs.max);
    }

    template <typename T>
    Clamped<T> operator * (const Clamped<T> lhs, const T rhs) {
        return Clamped(lhs.val * rhs, lhs.min, lhs.max);
    }

    template <typename T>
    Clamped<T> operator / (const Clamped<T> lhs, const T rhs) {
        return Clamped(lhs.val / rhs, lhs.min, lhs.max);
    }
}
}