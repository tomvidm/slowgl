#pragma once

#include <glm/glm.hpp>

namespace sgl { namespace math {
    class GridConfig {
    public:
    private:
        float tileUSize;
        float tileVSize;
    };
}
}