#pragma once

#include <array>

#include <glm/glm.hpp>

namespace sgl { namespace math {
    template <typename T>
    class Rect {
    public:
        Rect();
        Rect(T x0, T y0, T dx, T dy);
        Rect(const Rect<T>& other);

        std::array<glm::tvec2<T>, 4> cornerVectors() const;
    
        T left, top, width, height;
    };

    template <typename T>
    Rect<T>::Rect() 
    : left(T(0)), top(T(0)), width(T(0)), height(T(0)) {;}

    template <typename T>
    Rect<T>::Rect(T x0, T y0, T dx, T dy) 
    : left(x0), top(y0), width(dx), height(dy) {;}

    template <typename T>
    Rect<T>::Rect(const Rect<T>& other)
    : left(other.left), top(other.top), width(other.width), height(other.height) {;}

    template <typename T>
    std::array<glm::tvec2<T>, 4> Rect<T>::cornerVectors() const {
        return {
            glm::tvec2<T>(left, top),
            glm::tvec2<T>(left + width, top),
            glm::tvec2<T>(left + width, top + height),
            glm::tvec2<T>(left, top + height)
        };
    }
}
}