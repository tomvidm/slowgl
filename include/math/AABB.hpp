#pragma once

#include <glm/glm.hpp>

namespace sgl { namespace math {
    template <typename T>
    class AABB {
    public:
        AABB() = delete;
        AABB(const glm::vec3<T> corner, const glm::vec3<T> size)
        : corner(corner), size(size) {;}

        bool contains(const vec3<T> point) const;
    private:
        glm::vec3<T> corner;
        glm::vec3<T> size;
    };

    template <typename T>
    bool AABB::contains(const vec3<T> point) const {
        if (point.x < corner.x || point.x > corner.x + size.x ||
            point.y < corner.y || point.y > corner.y + size.y ||
            point.z < corner.z || point.z > corner.z + size.z) 
        {
            return false;
        } else {
            return true;
        }
    }
}
}