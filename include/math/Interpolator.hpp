#pragma once

namespace sgl { namespace math {
    template <typename T>
    T ease_start(const T s) {
        const T ss = s * s;
        return ss * (1 + s - ss);
    }

    template <typename T>
    T ease_end(const T s) {
        return 1.f - ease_start(1.f - s);
    }
}
}