#pragma once

#include <vector>

#include "animation/BoneState.hpp"

namespace sgl { namespace anim {
    struct SKeyFrame { 
        std::vector<BoneState> boneStates;
    };
}
}