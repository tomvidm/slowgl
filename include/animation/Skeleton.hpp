#pragma once

#include <vector>

#include <glm/glm.hpp>
#include <gtc/quaternion.hpp>
#include <gtc/matrix_transform.hpp>

#include "animation/BoneState.hpp"

namespace sgl { namespace anim {
    class Skeleton {
    public:
        std::vector<glm::mat4> getBoneTransforms() const;
    private:
        mutable std::vector<glm::mat4> transforms;

        std::vector<BoneState> bones;
        std::vector<int> boneIDs;
        std::vector<int> parentBoneIDs;
    };
}
}