#pragma once

#include <glm/glm.hpp>
#include <gtc/quaternion.hpp>

namespace sgl { namespace anim {
    struct BoneState {
        glm::quat orientation;
        glm::vec3 offset;
    };
}
}