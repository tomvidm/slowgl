#pragma once

#include "SDL.h"

#include <glm/glm.hpp>

namespace sgl { namespace input {
    struct MButtonEvent {
        enum class Trigger : size_t {
            Press,
            Release
        } trigger;

        enum class Type : size_t {
            Press,
            Release,
            Click,
            DoubleClick
        } type;

        glm::ivec2 mousePosition;
    };

    bool operator == (const MButtonEvent& lhs, const MButtonEvent& rhs);
}
}