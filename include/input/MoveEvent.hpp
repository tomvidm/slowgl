#pragma once

#include <glm/glm.hpp>

namespace sgl { namespace input {
    struct MoveEvent {
        glm::ivec2 mousePosition;
        glm::ivec2 mouseMotion;
    };

    bool operator == (const MoveEvent& lhs, const MoveEvent& rhs);
}
}