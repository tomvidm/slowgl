#pragma once

#include "SDL.h"

namespace sgl { namespace input {
    struct WindowEvent {
        enum class Type : size_t {
            Close,
            None
        } type;
    };

    bool operator == (const WindowEvent& lhs, const WindowEvent& rhs);

    WindowEvent makeWindowEvent(const SDL_Event& event);
}
}