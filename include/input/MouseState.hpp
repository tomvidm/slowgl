#pragma once

#include "input/MouseEvent.hpp"

#include "input/MButtonState.hpp"

namespace sgl { namespace input {
    enum class MButton : size_t {
        Left,
        Middle,
        Right,
        NUM_BUTTONS
    };

    class MouseState {
    public:
        MouseEvent onEvent(const SDL_Event& sdlEvent);
        MouseEvent onButtonEvent(const SDL_Event& sdlEvent);
        MouseEvent onWheelEvent(const SDL_Event& sdlEvent);
        MouseEvent onMoveEvent(const SDL_Event& sdlEvent);

        const MButtonState& getButtonState(const MButton button) const;
    private:
        MButtonState buttons[static_cast<size_t>(MButton::NUM_BUTTONS)];
    };
}
}