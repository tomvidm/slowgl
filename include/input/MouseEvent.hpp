#pragma once

#include "SDL.h"

#include "input/MButtonEvent.hpp"
#include "input/MoveEvent.hpp"

namespace sgl { namespace input {
    struct MouseEvent {
        enum class Type : size_t {
            Button,
            Wheel,
            Move
        } type;

        union {
            MButtonEvent mbutton;
            MoveEvent move;
        };
    };

    bool operator == (const MouseEvent& lhs, const MouseEvent& rhs);
}
}