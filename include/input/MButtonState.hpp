#pragma once

#include "SDL.h"

#include <glm/glm.hpp>

#include "MButtonEvent.hpp"

#include "Timer.hpp"

namespace sgl { namespace input {
    static const Time clickInterval(100);
    static const Time doubleClickInterval(200);

    class MButtonState {
    public:
        MButtonState();

        MButtonEvent onEvent(const SDL_Event& sdlEvent);
        MButtonEvent onPress(const SDL_Event& sdlEvent);
        MButtonEvent onRelease(const SDL_Event& sdlEvent);

        inline bool isPressed() const { return m_isPressed; }
    private:
        bool m_isPressed;
        Timer lastPressTimer;
        Timer lastReleaseTimer;
        glm::ivec2 lastPressPosition;
        glm::ivec2 lastReleasePosition;
    };
}
}