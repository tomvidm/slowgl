#pragma once

#include "SDL.h"

#include "input/WindowEvent.hpp"
#include "input/MouseEvent.hpp"

namespace sgl { namespace input {
    struct Event {
        enum class Type : size_t {
            MouseEvent,
            KeyboardEvent,
            JoystickEvent,
            ControllerEvent,
            WindowEvent,
            None
        } type;

        union {
            WindowEvent window;
            MouseEvent mouse;
            int placeholder;
        };
    };

    bool operator == (const Event& lhs, const Event& rhs);
}
}