#pragma once

#include <vector>

#include "input/Event.hpp"
#include "input/MouseState.hpp"

namespace sgl { namespace input {
    class EventHandler {
    public:
        const std::vector<Event>& getInputEvents();

        inline const MouseState& getMouseState() const { return mouseState; }
    private:
        void processEvents();

        mutable std::vector<Event> events;
        mutable MouseState mouseState;
    };
}
}