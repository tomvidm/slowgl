#pragma once

#include <glm/glm.hpp>

#include "OpenGLConfig.hpp"

namespace sgl {
    struct SlowGLConfig {
        glm::ivec2 windowSize;
        OpenGLConfig openGLConfig;
    };
}