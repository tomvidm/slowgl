#pragma once

#include <string>
#include <vector>

extern "C" {
    #include <GL/glew.h>
    #include <GL/gl.h>
    #include <GL/glu.h>
}

namespace sgl { namespace debug {
    class GLErrorReport {
    public:
        GLErrorReport();
    private:
        
    };
}
}