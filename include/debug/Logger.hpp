#pragma once

#include <string>
#include <iostream>
#include <vector>

#include "Time.hpp"

namespace sgl { namespace debug {

    struct LogEntry {
        Time timestamp;
        std::string message;
    };

    using LogList = std::vector<LogEntry>;

    class Logger {
    public:
        static void logMessage(const std::string& str);
    private:
        static std::vector<LogEntry> logList;
    };
}
}