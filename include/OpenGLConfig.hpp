#pragma once

namespace sgl {
    struct OpenGLConfig {
        unsigned openGLMajorVersion;
        unsigned openGLMinorVersion;
    };
}