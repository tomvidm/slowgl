#pragma once

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/glm.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_transform.hpp>


namespace sgl {
    class Transformable {
    public:
        Transformable();

        void setPosition(const glm::vec3 position);
        void setEulerAngles(const glm::vec3 eulerAngles);
        void setScale(const glm::vec3 scale);

        inline glm::vec3 getPosition() const { return m_position; }
        inline glm::vec3 getEulerAngles() const { return m_eulerAngles; }
        inline glm::vec3 getScale() const { return m_scale; }

        glm::mat4 getTransform() const;
        glm::mat4 getInverseTransform() const;
    private:
        glm::vec3 m_position;
        glm::vec3 m_eulerAngles;
        glm::vec3 m_scale;
        glm::vec3 m_origin;
        
        mutable bool transformNeedsUpdate;
        mutable bool inverseTransformNeedsUpdate;

        mutable glm::mat4 transform;
        mutable glm::mat4 inverseTransform;

    };
}