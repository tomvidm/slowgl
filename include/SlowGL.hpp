#pragma once

#include <memory>

extern "C" {
    #include <GL/glew.h>
    #include <GL/gl.h>
    #include <GL/glu.h>
}

#include "SDL.h"

#include "Camera.hpp"
#include "SlowGLConfig.hpp"

namespace sgl {
    class SlowGL {
    public:
        SlowGL() = delete;
        SlowGL(const SlowGLConfig& sglConfig);
        ~SlowGL();

        void setCamera(std::shared_ptr<Camera>& cam);
        void clearWindow();
        void drawToWindow();
    private:
        void initOpenGLContext(const OpenGLConfig& openGLConfig);

        SDL_Window* window;
        SDL_GLContext glContext;

        std::shared_ptr<Camera> camera;
    };

}