#pragma once

#include "SDL.h"

#include "Time.hpp"

namespace sgl {
    class Timer {
    public:
        Timer();

        Time restart();
        Time getElapsed() const;
    private:
        unsigned long t0;
    };
}