#include "Transformable.hpp"

namespace sgl {
    Transformable::Transformable()
    : m_position(glm::vec3(0.f, 0.f, 0.f)),
      m_eulerAngles(glm::vec3(0.f, 0.f, 0.f)),
      m_scale(glm::vec3(1.f, 1.f, 1.f)),
      m_origin(glm::vec3(0.f, 0.f, 0.f)),
      transformNeedsUpdate(true),
      inverseTransformNeedsUpdate(true) {;}

    void Transformable::setPosition(const glm::vec3 position) {
        m_position = position;
        transformNeedsUpdate = true;
        inverseTransformNeedsUpdate = true;
    }

    void Transformable::setEulerAngles(const glm::vec3 eulerAngles) {
        m_eulerAngles = eulerAngles;
        transformNeedsUpdate = true;
        inverseTransformNeedsUpdate = true;
    }

    void Transformable::setScale(const glm::vec3 scale) {
        m_scale = scale;
        transformNeedsUpdate = true;
        inverseTransformNeedsUpdate = true;
    }

    glm::mat4 Transformable::getTransform() const {
        if (transformNeedsUpdate) {
            glm::mat4 t = glm::mat4(1.f);
            t = glm::translate(t, -m_origin);
            t = glm::scale(t, m_scale) * glm::yawPitchRoll(m_eulerAngles.y, m_eulerAngles.x, m_eulerAngles.z);
            t = glm::translate(t, m_position + m_origin);

            transform = t;
            transformNeedsUpdate = false;
        }

        return transform;
    }

    glm::mat4 Transformable::getInverseTransform() const {
        if (inverseTransformNeedsUpdate) {
            inverseTransform = glm::inverse(getTransform());
            inverseTransformNeedsUpdate = false;
        }

        return inverseTransform;
    }
}