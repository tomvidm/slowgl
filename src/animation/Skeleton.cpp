#include "animation/Skeleton.hpp"

namespace sgl { namespace anim {
    std::vector<glm::mat4> Skeleton::getBoneTransforms() const {
        for (int boneID = 1; boneID < bones.size(); boneID++) {
            transforms[boneID] = transforms[parentBoneIDs[boneID]]
                               * glm::translate(glm::mat4(1.f), bones[boneID].offset)
                               * glm::mat4_cast(bones[boneID].orientation) 
                               * glm::translate(glm::mat4(1.f), -bones[boneID].offset);
        }
    }
}
}