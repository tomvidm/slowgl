#include "Time.hpp"

namespace sgl {
    Time::Time()
    : m_us(0) {;}

    Time::Time(const unsigned long us)
    : m_us(us) {;}

    Time::Time(const Time& other) 
    : m_us(other.m_us) {;}

    Time Time::microseconds(const unsigned long us) {
        return Time(us);
    }

    Time Time::milliseconds(const unsigned long ms) {
        return Time(1000 * ms);
    }

    Time Time::seconds(const float s) {
        return Time(static_cast<unsigned long>(1000000.f * s));
    }

    bool operator < (const Time& lhs, const Time& rhs) {
        return lhs.m_us < rhs.m_us;
    }

    bool operator == (const Time& lhs, const Time& rhs) {
        return lhs.m_us == rhs.m_us;
    }

    bool operator > (const Time& lhs, const Time& rhs) {
        return lhs.m_us > rhs.m_us;
    }

    Time operator + (const Time& lhs, const Time& rhs) {
        return Time(lhs.m_us + rhs.m_us);
    }

    Time operator - (const Time& lhs, const Time& rhs) {
        return Time(lhs.m_us - rhs.m_us);
    }
}