#include "graphics/VertexBuffer.hpp"

#include <iostream>
#include <glm/gtc/type_ptr.hpp>

namespace sgl { namespace graphics {
    VertexBuffer::VertexBuffer() {
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glGenBuffers(1, &verticesHandle);
        glGenBuffers(1, &normalsHandle);
        glGenBuffers(1, &indicesHandle);
        glGenBuffers(1, &colorsHandle);
        glBindVertexArray(0);
    }

    VertexBuffer::~VertexBuffer() {
        // glDeleteVertexArrays(1, &vao);
        // glDeleteBuffers(1, &verticesHandle);
        // glDeleteBuffers(1, &indicesHandle);
        // glDeleteBuffers(1, &colorsHandle);
    }

    void VertexBuffer::assignMesh(Mesh& mesh) {
        glBindVertexArray(vao);
        assignVertices(mesh.getVertices());
        assignColors(mesh.getColors());
        assignIndices(mesh.getIndices());
        glBindVertexArray(0);
    }

    void VertexBuffer::assignVertices(std::vector<glm::vec3>& pointsArray) {
        glBindVertexArray(vao);
        numVertices = pointsArray.size();
        verticesPtr = pointsArray.data();
        glBindBuffer(GL_ARRAY_BUFFER, verticesHandle);
        glBufferData(GL_ARRAY_BUFFER, 3 * numVertices * sizeof(glm::vec3), verticesPtr, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glBindVertexArray(0);
    }

    void VertexBuffer::assignNormals(std::vector<glm::vec3>& normalsArray) {
        glBindVertexArray(vao);
        numNormals = normalsArray.size();
        normalsPtr = normalsArray.data();
        glBindBuffer(GL_ARRAY_BUFFER, normalsHandle);
        glBufferData(GL_ARRAY_BUFFER, 3 * numNormals * sizeof(glm::vec3), normalsPtr, GL_STATIC_DRAW);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glBindVertexArray(0);
    }

    void VertexBuffer::assignColors(std::vector<Color>& colorsArray) {
        glBindVertexArray(vao);
        numColors = colorsArray.size();
        colorsPtr = colorsArray.data();
        glBindBuffer(GL_ARRAY_BUFFER, colorsHandle);
        glBufferData(GL_ARRAY_BUFFER, numColors * sizeof(Color), colorsPtr, GL_STATIC_DRAW);
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 0, 0);
        glBindVertexArray(0);
    }

    void VertexBuffer::assignIndices(std::vector<unsigned>& indexArray) {
        glBindVertexArray(vao);
        numIndices = indexArray.size();
        indicesPtr = indexArray.data();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesHandle);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(unsigned), indicesPtr, GL_STATIC_DRAW);
        glBindVertexArray(0);
    }

    void VertexBuffer::assignParentBones(std::vector<unsigned>& parentBoneArray) {
        glBindVertexArray(vao);
        glGenBuffers(1, &parentBoneIDHandle);
        parentBoneIDPtr = parentBoneArray.data();
        glBindBuffer(GL_ARRAY_BUFFER, parentBoneIDHandle);
        glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(unsigned), parentBoneIDPtr, GL_STATIC_DRAW);
        glBindVertexArray(0);
    }

    void VertexBuffer::assignPrentBoneWeights(std::vector<float>& parentBoneWeights) {
        glBindVertexArray(vao);
        glGenBuffers(1, &parentBoneWeightsHandle);
        parentBoneWeightsPtr = parentBoneWeights.data();
        glBindBuffer(GL_ARRAY_BUFFER, parentBoneWeightsHandle);
        glBufferData(GL_ARRAY_BUFFER, numVertices * sizeof(float), parentBoneWeightsPtr, GL_STATIC_DRAW);
        glBindVertexArray(0);
    }

    void VertexBuffer::draw(std::shared_ptr<Camera> camera, RenderState state, GLuint primitiveType) const {
        glBindVertexArray(vao);
        
        state.shader->use();
        GLuint modelMat =       glGetUniformLocation(state.shader->getProgramID(), "modelMatrix");
        GLuint viewMat =        glGetUniformLocation(state.shader->getProgramID(), "viewMatrix");
        GLuint projMat =        glGetUniformLocation(state.shader->getProgramID(), "projMatrix");
        GLuint viewProjMat =    glGetUniformLocation(state.shader->getProgramID(), "viewProjMatrix");
        GLuint normalMat =      glGetUniformLocation(state.shader->getProgramID(), "normalMatrix");

        const glm::mat3 normalMatrix = glm::transpose(glm::inverse(glm::mat3(state.transform)));

        glUniformMatrix4fv(modelMat,      1, GL_FALSE, glm::value_ptr(state.transform));
        glUniformMatrix4fv(projMat,       1, GL_FALSE, glm::value_ptr(camera->getProjectionMatrix()));
        glUniformMatrix4fv(viewMat,       1, GL_FALSE, glm::value_ptr(camera->getViewMatrix()));
        glUniformMatrix4fv(viewProjMat,   1, GL_FALSE, glm::value_ptr(camera->getViewProjMatrix()));
        glUniformMatrix3fv(normalMat,     1, GL_FALSE, glm::value_ptr(normalMatrix));

        glDrawElements(primitiveType, numIndices, GL_UNSIGNED_INT, 0);

        glBindVertexArray(0);
    }
}
}