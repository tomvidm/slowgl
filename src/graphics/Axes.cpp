#include "graphics/Axes.hpp"

namespace sgl { namespace graphics {
    Axes::Axes() {
        static std::vector<glm::vec3> vertices = {
            glm::vec3(0.f, 0.f, 0.f), glm::vec3(1.f, 0.f, 0.f),
            glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 1.f, 0.f),
            glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 0.f, 1.f),
            glm::vec3(0.f, 0.f, 0.f), glm::vec3(-1.f, 0.f, 0.f),
            glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, -1.f, 0.f),
            glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 0.f, -1.f)
        };

        static std::vector<glm::vec4> colors = {
            glm::vec4(1.f, 0.f, 0.f, 1.f), glm::vec4(1.f, 0.f, 0.f, 1.f),
            glm::vec4(0.f, 1.f, 0.f, 1.f), glm::vec4(0.f, 1.f, 0.f, 1.f),
            glm::vec4(0.f, 0.f, 1.f,  1.f), glm::vec4(0.f, 0.f, 1.f, 1.f),
            glm::vec4(0.5f, 0.f, 0.f, 0.75f), glm::vec4(0.5f, 0.f, 0.f, 0.75f),
            glm::vec4(0.f, 0.5f, 0.f, 0.75f), glm::vec4(0.f, 0.5f, 0.f, 0.75f),
            glm::vec4(0.f, 0.f, 0.5f, 0.75f), glm::vec4(0.f, 0.f, 0.5f, 0.75f)
        };

        static std::vector<unsigned> indices = {
            0, 1, 2, 3, 4, 5,
            6, 7, 8, 9, 10, 11
        };

        vbuf.assignVertices(vertices);
        vbuf.assignColors(colors);
        vbuf.assignIndices(indices);
    }

    void Axes::draw(std::shared_ptr<Camera> camera, RenderState state) const {
        state.transform *= getTransform();
        vbuf.draw(camera, state, GL_LINES);
    }
}
}