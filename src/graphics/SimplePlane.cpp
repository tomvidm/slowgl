#include "graphics/SimplePlane.hpp"

#include <glm/gtc/type_ptr.hpp>

namespace sgl { namespace graphics {
    void SimplePlane::draw(std::shared_ptr<Camera> camera, RenderState state) const {
        //state.transform *= getTransform();
        //state.shader->use();
        
        GLuint loc = glGetUniformLocation(state.shader->getProgramID(), "MVPMatrix");
        glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(camera->getViewProjMatrix()));
        //glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(glm::mat4(1.f)));

        glColor3f(1.f, 0.f, 0.f);
        glBegin(GL_QUADS);
            glVertex3f(-1.f, 1.f, -11.f);
            glVertex3f(11.f, 0.f, -1.f);
            glVertex3f(1.f, 1.f, 1.f);
            glVertex3f(-1.f, 0.f, 11.f);
        glEnd();
    }
}
}