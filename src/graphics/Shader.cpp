#include "graphics/Shader.hpp"

#include "debug/Logger.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>

namespace sgl { namespace graphics {
    Shader::Shader(const std::string& pathToVert,
                   const std::string& pathToFrag) {
        debug::Logger::logMessage(
            std::string("Shader: Loading shaders from\n\t") +
            std::string(pathToVert) + std::string("\n\t") +
            std::string(pathToFrag)
        );
        // 1. retrieve the vertex/fragment source code from filePath
        std::string vertexCode;
        std::string fragmentCode;
        std::ifstream vShaderFile;
        std::ifstream fShaderFile;
        // ensure ifstream objects can throw exceptions:
        vShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
        fShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
        try 
        {
            // open files
            vShaderFile.open(pathToVert);
            fShaderFile.open(pathToFrag);
            std::stringstream vShaderStream, fShaderStream;
            // read file's buffer contents into streams
            vShaderStream << vShaderFile.rdbuf();
            fShaderStream << fShaderFile.rdbuf();		
            // close file handlers
            vShaderFile.close();
            fShaderFile.close();
            // convert stream into string
            vertexCode   = vShaderStream.str();
            fragmentCode = fShaderStream.str();		
        }
        catch(std::ifstream::failure e)
        {
            debug::Logger::logMessage(
                std::string("Shader error: File not read!")
            );
        }
        const char* vShaderCode = vertexCode.c_str();
        const char* fShaderCode = fragmentCode.c_str();

        // 2. compile shaders
        unsigned int vertex, fragment;
        int success;
        char infoLog[512];
        
        // vertex Shader
        vertex = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertex, 1, &vShaderCode, NULL);
        glCompileShader(vertex);
        // print compile errors if any
        glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
        if(!success)
        {
            glGetShaderInfoLog(vertex, 512, NULL, infoLog);
            debug::Logger::logMessage(
                std::string("Shader error: Vertex shader compilation failed!\n") +
                std::string(infoLog)
            );
        };
        
        // similiar for Fragment Shader
        fragment = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragment, 1, &fShaderCode, NULL);
        glCompileShader(fragment);
        glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
        if (!success) {
            glGetShaderInfoLog(fragment, 512, NULL, infoLog);
            debug::Logger::logMessage(
                std::string("Shader error: Fragment shader compilation failed!\n") +
                std::string(infoLog)
            );
        }
        
        // shader Program
        shaderProgramID = glCreateProgram();
        glAttachShader(shaderProgramID, vertex);
        glAttachShader(shaderProgramID, fragment);
        glLinkProgram(shaderProgramID);
        // print linking errors if any
        glGetProgramiv(shaderProgramID, GL_LINK_STATUS, &success);
        if(!success)
        {
            glGetProgramInfoLog(shaderProgramID, 512, NULL, infoLog);
            debug::Logger::logMessage(
                std::string("Shader error: Linking failed!\n") +
                std::string(infoLog)
            );
        }
        
        // delete the shaders as they're linked into our program now and no longer necessery
        glDeleteShader(vertex);
        glDeleteShader(fragment);

        modelMat =       glGetUniformLocation(getProgramID(), "modelMatrix");
        viewMat =        glGetUniformLocation(getProgramID(), "viewMatrix");
        projMat =        glGetUniformLocation(getProgramID(), "projMatrix");
        viewProjMat =    glGetUniformLocation(getProgramID(), "viewProjMatrix");
        normalMat =      glGetUniformLocation(getProgramID(), "normalMatrix");
    }

    void Shader::use() const {
        glUseProgram(shaderProgramID);
    }

    void Shader::setModelMatrix(const glm::mat4& mview) {
        glUniformMatrix4fv(modelMat, 1, GL_FALSE, glm::value_ptr(mview));
        glUniformMatrix3fv(normalMat, 1, GL_FALSE, glm::value_ptr(
            glm::transpose(glm::inverse(glm::mat3(mview)))
        ));
    }

    void Shader::setCamera(std::shared_ptr<Camera>& camera) {
        glUniformMatrix4fv(viewMat, 1, GL_FALSE, glm::value_ptr(camera->getViewMatrix()));
        glUniformMatrix4fv(projMat, 1, GL_FALSE, glm::value_ptr(camera->getProjectionMatrix()));
        glUniformMatrix4fv(viewProjMat, 1, GL_FALSE, glm::value_ptr(camera->getViewProjMatrix()));
    }

    void Shader::setUint(const char* uniformID, unsigned in) {
        GLuint id = glGetUniformLocation(shaderProgramID, uniformID);
        glUniform1ui(id, in);
        if (glGetError() == GL_INVALID_OPERATION) {
            debug::Logger::logMessage(
                std::string("Shader::setUint error ")
            );
        }
    }

    void Shader::setInt(const char* uniformID, int in) {
        GLuint id = glGetUniformLocation(shaderProgramID, uniformID);
        glUniform1i(id, in);
        if (glGetError() == GL_INVALID_OPERATION) {
            debug::Logger::logMessage(
                std::string("Shader::setInt error ")
            );
        }
    }

    void Shader::setFloat(const char* uniformID, float in) {
        GLuint id = glGetUniformLocation(shaderProgramID, uniformID);
        glUniform1f(id, in);
        if (glGetError() == GL_INVALID_OPERATION) {
            debug::Logger::logMessage(
                std::string("Shader::setFloat error ")
            );
        }
    }

    void Shader::setMat3(const char* uniformID, glm::mat3& in) {
        GLuint id = glGetUniformLocation(shaderProgramID, uniformID);
        glUniformMatrix3fv(id, 1, GL_FALSE, glm::value_ptr(in));
        if (glGetError() == GL_INVALID_OPERATION) {
            debug::Logger::logMessage(
                std::string("Shader::setMat3 error ")
            );
        }
    }

    void Shader::setMat3(const char* uniformID, std::vector<glm::mat3>& invec) {
        GLuint id = glGetUniformLocation(shaderProgramID, uniformID);
        glUniformMatrix3fv(id, invec.size(), GL_FALSE, reinterpret_cast<GLfloat*>(invec.data()));
        if (glGetError() == GL_INVALID_OPERATION) {
            debug::Logger::logMessage(
                std::string("Shader::setMat3 array error ")
            );
        }
    }

    void Shader::setMat4(const char* uniformID, glm::mat4& in) {
        GLuint id = glGetUniformLocation(shaderProgramID, uniformID);
        glUniformMatrix4fv(id, 1, GL_FALSE, glm::value_ptr(in));
        if (glGetError() == GL_INVALID_OPERATION) {
            debug::Logger::logMessage(
                std::string("Shader::setMat4 array error ")
            );
        }
    }

    void Shader::setMat4(const char* uniformID, std::vector<glm::mat4>& invec) {
        GLuint id = glGetUniformLocation(shaderProgramID, uniformID);
        glUniformMatrix4fv(id, invec.size(), GL_FALSE, reinterpret_cast<GLfloat*>(invec.data()));
        if (glGetError() == GL_INVALID_OPERATION) {
            debug::Logger::logMessage(
                std::string("Shader::setMat4 array error ")
            );
        }
    }

    void Shader::setVec3(const char* uniformID, glm::vec3& in) {
        GLuint id = glGetUniformLocation(shaderProgramID, uniformID);
        glUniform3fv(id, 1, glm::value_ptr(in));
        if (glGetError() == GL_INVALID_OPERATION) {
            debug::Logger::logMessage(
                std::string("Shader::setVec3 error ")
            );
        }
    }

    void Shader::setVec3(const char* uniformID, std::vector<glm::vec3>& invec) {
        GLuint id = glGetUniformLocation(shaderProgramID, uniformID);
        glUniform3fv(id, invec.size(), reinterpret_cast<GLfloat*>(invec.data()));
        if (glGetError() == GL_INVALID_OPERATION) {
            debug::Logger::logMessage(
                std::string("Shader::setVec3 array error ")
            );
        }
    }

    void Shader::setVec4(const char* uniformID, glm::vec4& in) {
        GLuint id = glGetUniformLocation(shaderProgramID, uniformID);
        glUniform4fv(id, 1, glm::value_ptr(in));
        if (glGetError() == GL_INVALID_OPERATION) {
            debug::Logger::logMessage(
                std::string("Shader::setVec4 error ")
            );
        }
    }

    void Shader::setVec4(const char* uniformID, std::vector<glm::vec4>& invec) {
        GLuint id = glGetUniformLocation(shaderProgramID, uniformID);
        glUniform4fv(id, invec.size(), reinterpret_cast<GLfloat*>(invec.data()));
        if (glGetError() == GL_INVALID_OPERATION) {
            debug::Logger::logMessage(
                std::string("Shader::setVec4 array error ")
            );
        }
    }
}
}