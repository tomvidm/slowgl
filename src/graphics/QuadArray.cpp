#include "graphics/QuadArray.hpp"

#include <iostream>

namespace sgl { namespace graphics {
    QuadArray::QuadArray()
    : toBufferData(false) {
        glGenVertexArrays(1, &vaoID);
            glBindVertexArray(vaoID);
            glGenBuffers(1, &vertexID);
            glGenBuffers(1, &elementsID);
        glBindVertexArray(0);
    }

    size_t QuadArray::addQuad(const Quad& quad) {
        const size_t v = vertices.size();
        const size_t i = indices.size();
        vertices.resize(v + 4);
        indices.resize(i + 6);

        const glm::mat4 tform =
            glm::translate(glm::mat4(1.f), glm::vec3(quad.origin.x, 0.f, quad.origin.y) + quad.position) *
            glm::orientation(quad.normal, glm::vec3(0.f, 1.f, 0.f)) *
            glm::scale(glm::mat4(1.f), glm::vec3(quad.scale.x, 0.f, quad.scale.y)) *
            glm::translate(glm::mat4(1.f), glm::vec3(-quad.origin.x, 0.f, -quad.origin.y));

        const glm::vec3 transformedNormal = glm::vec3(tform * glm::vec4(quad.normal, 1.f));

        vertices[v].position = glm::vec3(tform * glm::vec4(0.f, 0.f, 0.f, 1.f));
        vertices[v].normal = quad.normal;
        vertices[v].color = quad.color;

        vertices[v + 1].position = glm::vec3(tform * glm::vec4(1.f, 0.f, 0.f, 1.f));
        vertices[v + 1].normal = quad.normal;
        vertices[v + 1].color = quad.color;
        
        vertices[v + 2].position = glm::vec3(tform * glm::vec4(1.f, 0.f, 1.f, 1.f));
        vertices[v + 2].normal = quad.normal;
        vertices[v + 2].color = quad.color;
        
        vertices[v + 3].position = glm::vec3(tform * glm::vec4(0.f, 0.f, 1.f, 1.f));
        vertices[v + 3].normal = quad.normal;
        vertices[v + 3].color = quad.color;

        indices.resize(i + 6);

        indices[i] = v;
        indices[i + 1] = v + 2;
        indices[i + 2] = v + 3;
        indices[i + 3] = v;
        indices[i + 4] = v + 1;
        indices[i + 5] = v + 2;

        toBufferData = true;
        return i / 4;
    }

    void QuadArray::bufferVertices() const {
        glBindBuffer(GL_ARRAY_BUFFER, vertexID);
            glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);
                glEnableVertexAttribArray(0);
                glEnableVertexAttribArray(1);
                glEnableVertexAttribArray(2);
                glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, position)));
                glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, normal)));
                glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<void*>(offsetof(Vertex, color)));
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementsID);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned), indices.data(), GL_STATIC_DRAW);
        toBufferData = false;
    }

    void QuadArray::draw(std::shared_ptr<Camera> camera, RenderState state) const {
        glBindVertexArray(vaoID);
            state.shader->use();

            if (toBufferData) {
                bufferVertices();
            }
        
        state.shader->setModelMatrix(state.transform);
        state.shader->setCamera(camera);

        glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, reinterpret_cast<void*>(0));

        glBindVertexArray(0);
    }
}
}