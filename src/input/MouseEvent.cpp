#include "input/MouseEvent.hpp"

namespace sgl { namespace input {
    bool operator == (const MouseEvent& lhs, const MouseEvent& rhs) {
        if (lhs.type == rhs.type) {
            switch (lhs.type) {
                case MouseEvent::Type::Button:
                    return lhs.mbutton == rhs.mbutton;
                case MouseEvent::Type::Wheel:
                case MouseEvent::Type::Move:
                default:
                    return false;
            }
        } else {
            return false;
        }
    }
}
}