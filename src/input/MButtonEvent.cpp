#include "input/MButtonEvent.hpp"

namespace sgl { namespace input {
    // Only compared the type data member. Mouse position of the event is ignored.
    bool operator == (const MButtonEvent& lhs, const MButtonEvent& rhs) {
        if (lhs.type == rhs.type) {
            return true;
        } else {
            return false;
        }
    }
}
}