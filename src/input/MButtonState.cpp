#include "input/MButtonState.hpp"

namespace sgl { namespace input {
    MButtonState::MButtonState() 
    : m_isPressed(false),
      lastPressTimer(),
      lastReleaseTimer(),
      lastPressPosition(),
      lastReleasePosition() {;}

    MButtonEvent MButtonState::onEvent(const SDL_Event& sdlEvent) {
        switch (sdlEvent.button.state) {
            case SDL_PRESSED:
                return onPress(sdlEvent);
            case SDL_RELEASED:
                return onRelease(sdlEvent);
            default:
                return MButtonEvent();
        }
    }

    MButtonEvent MButtonState::onPress(const SDL_Event& sdlEvent) {
        const SDL_MouseButtonEvent& sdlButtonEvent = sdlEvent.button;
        m_isPressed = true;
        lastPressPosition = glm::ivec2(sdlButtonEvent.x, sdlButtonEvent.y);
        MButtonEvent buttonEvent;
        buttonEvent.mousePosition = lastPressPosition;
        buttonEvent.trigger = MButtonEvent::Trigger::Press;

        if (lastPressTimer.restart() < doubleClickInterval) {
            buttonEvent.type = MButtonEvent::Type::DoubleClick;
        } else {
            buttonEvent.type = MButtonEvent::Type::Press;
        }

        return buttonEvent;
    }

    MButtonEvent MButtonState::onRelease(const SDL_Event& sdlEvent) {
        const SDL_MouseButtonEvent& sdlButtonEvent = sdlEvent.button;
        m_isPressed = false;
        lastReleasePosition = glm::ivec2(sdlButtonEvent.x, sdlButtonEvent.y);
        MButtonEvent buttonEvent;
        buttonEvent.mousePosition = lastPressPosition;
        buttonEvent.trigger = MButtonEvent::Trigger::Release;

        if (lastPressTimer.restart() < clickInterval) {
            buttonEvent.type = MButtonEvent::Type::Click;
        } else {
            buttonEvent.type = MButtonEvent::Type::Release;
        }

        return buttonEvent;
    }
}
}