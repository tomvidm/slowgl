#include "input/WindowEvent.hpp"

namespace sgl { namespace input {
    bool operator == (const WindowEvent& lhs, const WindowEvent& rhs) {
        if (lhs.type == rhs.type) {
            return true;
        } else {
            return false;
        }
    }

    WindowEvent makeWindowEvent(const SDL_Event& event) {
        WindowEvent windowEvent;
        switch (event.type) {
            case SDL_QUIT:
                windowEvent.type = WindowEvent::Type::Close;
                return windowEvent;
            default:
                windowEvent.type = WindowEvent::Type::None;
                return windowEvent;
        }
    }
}
}