#include "input/MouseState.hpp"

namespace sgl { namespace input {
    MouseEvent MouseState::onEvent(const SDL_Event& sdlEvent) {
        MouseEvent mouseEvent;
        switch (sdlEvent.type) {
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
                mouseEvent.type = MouseEvent::Type::Button;
                mouseEvent.mbutton = buttons[static_cast<size_t>(sdlEvent.button.button)].onEvent(sdlEvent);
                return mouseEvent;
            case SDL_MOUSEMOTION:
                mouseEvent.type = MouseEvent::Type::Move;
                mouseEvent.move = MoveEvent{
                    glm::ivec2(sdlEvent.motion.x, sdlEvent.motion.y),
                    glm::ivec2(sdlEvent.motion.xrel, sdlEvent.motion.yrel)
                };
                return mouseEvent;
            default:
                return MouseEvent();
        }
    }

    const MButtonState& MouseState::getButtonState(const MButton button) const {
        return buttons[static_cast<size_t>(button)];
    }
}
}