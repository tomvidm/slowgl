#include "input/MoveEvent.hpp"

namespace sgl { namespace input {
    bool operator == (const MoveEvent& lhs, const MoveEvent& rhs) {
        if (lhs.mouseMotion == rhs.mouseMotion) {
            return true;
        } else {
            return false;
        }
    }
}
}