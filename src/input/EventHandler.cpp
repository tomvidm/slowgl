#include "SDL.h"

#include "input/EventHandler.hpp"

namespace sgl { namespace input {
    const std::vector<Event>& EventHandler::getInputEvents() {
        processEvents();
        return events;
    }

    void EventHandler::processEvents() {
        events.clear();
        SDL_Event sdlEvent;
        Event event;
        while (SDL_PollEvent(&sdlEvent) != 0) {
            switch (sdlEvent.type) {
                case SDL_QUIT:
                    event.type = Event::Type::WindowEvent;
                    event.window = WindowEvent{ WindowEvent::Type::Close };
                    break;
                case SDL_MOUSEBUTTONDOWN:
                case SDL_MOUSEBUTTONUP:
                case SDL_MOUSEMOTION:
                    event.type = Event::Type::MouseEvent;
                    event.mouse = mouseState.onEvent(sdlEvent);
                default:
                    break;
            }
            events.push_back(event);
        }
    }
}
}