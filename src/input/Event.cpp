#include "input/Event.hpp"

namespace sgl { namespace input {
    bool operator == (const Event& lhs, const Event& rhs) {
        if (lhs.type == rhs.type) {
            switch (lhs.type) {
                case Event::Type::MouseEvent:
                    return lhs.mouse == rhs.mouse;
                case Event::Type::KeyboardEvent:
                case Event::Type::JoystickEvent:
                case Event::Type::ControllerEvent:
                case Event::Type::WindowEvent:
                    return lhs.window == rhs.window;
                default:
                    return false;
            }
        } else {
            return false;
        }
    }
}
}