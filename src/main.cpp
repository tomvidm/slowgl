#include <iostream>

#include "debug/Logger.hpp"
#include "math/Rect.hpp"
#include "graphics/Axes.hpp"
#include "graphics/Shader.hpp"
#include "graphics/SimplePlane.hpp"
#include "graphics/RenderState.hpp"
#include "graphics/QuadArray.hpp"
#include "Timer.hpp"
#include "Camera.hpp"
#include "SlowGL.hpp"
#include "graphics/Vertex.hpp"
#include "Mesh.hpp"
#include "input/EventHandler.hpp"
#include "graphics/VertexBuffer.hpp"

#include <glm/gtc/matrix_transform.hpp>

float quasirand(int a, int b) {
    static int i = 231487;
    float result = static_cast<float>(
        (2318879 * a + 2131 * b ) % 1000
    ) / 1000.f;

    return result;
}

int main(int argc, char* argv[]) {
    using sgl::graphics::RenderState;
    using sgl::graphics::Shader;
    using sgl::graphics::VertexBuffer;
    using sgl::Mesh;
    sgl::SlowGLConfig sglConfig = sgl::SlowGLConfig {
        glm::ivec2(800, 600),
        sgl::OpenGLConfig{
                3, 3
        }
    };

    sgl::SlowGL sgl(sglConfig);
    sgl::input::EventHandler eventHandler;
    
    sgl::Timer subframeTimer;
    sgl::Time frameTime = sgl::Time::milliseconds(16);
    sgl::Timer frameTimer;

    std::shared_ptr<Shader> defaultShader = std::make_shared<Shader>(
        std::string("../resources/shaders/defaultVertShader.vert"),
        std::string("../resources/shaders/defaultFragShader.frag")
    );

    std::shared_ptr<Shader> debugShader = std::make_shared<Shader>(
        std::string("../resources/shaders/debugVertShader.vert"),
        std::string("../resources/shaders/debugFragShader.frag")
    );

    RenderState state {
        glm::mat4(1.f),
        defaultShader
    };



    std::vector<glm::vec4> intensities = {
        glm::vec4(2.f, 0.5f, 0.5f, 1.f),
        glm::vec4(0.f, 0.f, 2.f, 1.f)
    };

    std::vector<glm::vec3> positions = {
        glm::vec3(0.f, 1.f, 0.f),
        glm::vec3(-2.f, 2.f, -3.f)
    };

    defaultShader->use();
    defaultShader->setInt("numLights", 2);
    defaultShader->setVec3("lightPositions", positions);
    defaultShader->setVec4("lightIntensities", intensities);

    debugShader->use();
    debugShader->setInt("numLights", 2);
    debugShader->setVec3("lightPositions", positions);
    debugShader->setVec4("lightIntensities", intensities);


    std::shared_ptr<sgl::Camera> cam = std::make_shared<sgl::Camera>();
    cam->setFOV(75.f);
    cam->setAspectRatio(3.f, 3.f);
    cam->setNearClipPlane(0.1f);
    cam->setFarClipPlane(100.f);

    float s = 0.f;
    cam->setPosition(glm::vec3(2.f, 5.f, 2.f));
    cam->lookat(glm::vec3(0.f, 0.f, 0.f));

    sgl.setCamera(cam);

    sgl::graphics::Axes axes;
    axes.setScale(glm::vec3(10.f, 10.f, 10.f));

    sgl::graphics::QuadArray qarr;

    for (int xi = -25; xi < 25; xi++) {
        for (int yi = -25; yi < 25; yi++) {
            sgl::graphics::Quad quad = {
                glm::vec3(static_cast<float>(xi), 0.f, static_cast<float>(yi)),
                glm::normalize(glm::vec3(
                    quasirand(xi, yi) / 4.f, 
                    1.f, 
                    quasirand(yi - 12313, xi + 2414) / 3.f)),
                glm::vec4(0.5f, 0.5f, 0.5f, 1.f),
                glm::vec2(0.94f, 0.94f),
                glm::vec2(0.f, 0.f)
            };

            qarr.addQuad(quad);
        }
    }

    bool running = true;
    while (running) {
        if (frameTimer.getElapsed() < frameTime) {
            continue;
        }
        const float dt = frameTimer.restart().asSeconds();
        for (auto& event : eventHandler.getInputEvents()) {
            if (event.type == sgl::input::Event::Type::WindowEvent) {
                if (event.window.type == sgl::input::WindowEvent::Type::Close) {
                    running = false;
                }
            } else if (event.type == sgl::input::Event::Type::MouseEvent) {
                if (event.mouse.type == sgl::input::MouseEvent::Type::Move) {
                    const float r = 0.25f;
                    const float x = static_cast<float>(event.mouse.move.mouseMotion.x);
                    const float y = static_cast<float>(event.mouse.move.mouseMotion.y);
                    cam->rotateViewportX(x * r * dt);
                    cam->rotateViewportY(y * r * dt);
                }
            }
        }
        qarr.setScale(glm::vec3(0.1f, 0.1f, 0.1f));

        s += 0.01f * dt;
        sgl.clearWindow();
        state.shader = defaultShader;
        qarr.draw(cam, state);
        state.shader = debugShader;
        axes.draw(cam, state);
        sgl.drawToWindow();
    }

    return 0;
}