#include "debug/Logger.hpp"

#include "SlowGL.hpp"

namespace sgl {
    SlowGL::SlowGL(const SlowGLConfig& sglConfig) {
        // Init SDL
        SDL_Init(
            SDL_INIT_EVENTS ||
            SDL_INIT_VIDEO
        );

        SDL_CaptureMouse(SDL_TRUE);

        debug::Logger::logMessage(
            std::string("SDL Events and Video Initialized.")
        );

        // Create the main window
        window = SDL_CreateWindow(
            "SlowGL",
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            sglConfig.windowSize.x,
            sglConfig.windowSize.y,
            SDL_WINDOW_OPENGL
        );

        initOpenGLContext(sglConfig.openGLConfig);

        debug::Logger::logMessage(
            std::string("SDL Window created with OpenGL flag.")
        );
    }

    SlowGL::~SlowGL() {
        debug::Logger::logMessage(
            std::string("SDL and OpenGL contexts destroyed on exit.")
        );

        SDL_GL_DeleteContext(glContext);
        SDL_DestroyWindow(window);
        SDL_Quit();

    }

    void SlowGL::initOpenGLContext(const OpenGLConfig& openGLConfig) {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, openGLConfig.openGLMajorVersion);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, openGLConfig.openGLMinorVersion);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        glFrontFace(GL_CW);

        glContext = SDL_GL_CreateContext(window);
        SDL_GL_MakeCurrent(window, glContext);
        glewExperimental = true;
        glewInit();

        glEnable(GL_DEPTH_TEST);
        glDepthMask(GL_TRUE);
        glDepthFunc(GL_LEQUAL);
        glDepthRange(0.0f, 1.0f);

        glEnable(GL_DEBUG_OUTPUT);

        debug::Logger::logMessage(
            std::string("Initialized OpenGL Context.")
        );
    }

    void SlowGL::setCamera(std::shared_ptr<Camera>& cam) {
        camera = cam;
    }

    void SlowGL::clearWindow() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        // Set unitform view-projection matrix
    }

    void SlowGL::drawToWindow() {
        SDL_GL_SwapWindow(window);
    }

}