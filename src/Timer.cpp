#include "Timer.hpp"

namespace sgl {
    Timer::Timer()
    : t0(0) {;}

    Time Timer::restart() {
        const unsigned long current_us = 1000 * SDL_GetTicks();
        const Time elapsed = Time::microseconds(current_us - t0);
        t0 = current_us;
        return elapsed;
    }

    Time Timer::getElapsed() const {
        return Time(1000 * SDL_GetTicks() - t0); 
    }
}