#include "SDL.h"

#include "debug/Logger.hpp"

namespace sgl { namespace debug {
    std::vector<LogEntry> Logger::logList = {};

    void Logger::logMessage(const std::string& str) {
        logList.push_back(
            LogEntry{
                Time::milliseconds(SDL_GetTicks()),
                str
            }
        );
        std::cout << logList.back().timestamp.asSeconds() << " - " << logList.back().message << std::endl;
    }
}
}