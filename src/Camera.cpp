#include "Camera.hpp"

#define GLM_ENABLE_EXPERIMENTAL

#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>


namespace sgl {
    Camera::Camera() 
    : m_origin(0.f, 0.f, 0.f),
      m_position(0.f, 0.f, 0.f),
      m_eulerAngles(0.f, 0.f, 0.f),
      m_dir(0.f, 0.f, -1.f),
      m_view_x(1.f, 0.f, 0.f),
      m_view_y(0.f, 1.f, 0.f),
      fov(60.f),
      aspect(4.f / 3.f),
      nearClippingPlane(0.1f),
      farClippingPlane(100.f),
      projMatrixNeedsUpdate(true),
      viewMatrixNeedsUpdate(true),
      compositeMatrixNeedsUpdate(true) {;}

    void Camera::setOrigin(const glm::vec3 origin) {
        m_origin = origin;
        viewMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }

    void Camera::setPosition(const glm::vec3 position) {
        m_position = position;
        viewMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }

    void Camera::setDirection(const glm::vec3 direction) {
        m_dir = direction;
        viewMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }

    void Camera::setEulerAngles(const glm::vec3 eulerAngles) {
        m_eulerAngles = eulerAngles;
        viewMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }

    void Camera::move(const glm::vec3 displacement) {
        m_position += displacement;
        viewMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }

    void Camera::lookat(const glm::vec3 target) {
        m_dir = glm::normalize(target - m_position);
        viewMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }

    void Camera::rotate(const glm::vec3 axis, const float deg) {
        m_dir = glm::rotate(m_dir, deg, axis);
        m_view_x = glm::rotate(m_dir, deg, axis);
        m_view_y = glm::rotate(m_dir, deg, axis);
        viewMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }

    void Camera::rotateViewportX(const float deg) {
        //rotate(m_view_y, deg);
        m_dir = glm::rotate(m_dir, deg, -m_view_y);
        m_view_x = glm::rotate(m_view_x, deg, -m_view_y);
        viewMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }

    void Camera::rotateViewportY(const float deg) {
        //rotate(m_view_x, deg);
        m_dir = glm::rotate(m_dir, deg, -m_view_x);
        m_view_y = glm::rotate(m_view_y, deg, -m_view_x);
        viewMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }

    glm::vec3 Camera::getViewportX() const {
        return m_view_x;
    }

    glm::vec3 Camera::getViewportY() const {
        return m_view_y;
    }

    void Camera::setFOV(const float angle) {
        fov = angle;
        projMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }

    void Camera::setAspectRatio(const float height, const float width) {
        aspect = width / height;
        projMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }
    
    void Camera::setAspectRatio(const float aspectRatio) {
        aspect = aspectRatio;
        projMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }
    
    void Camera::setNearClipPlane(const float nearZ) {
        nearClippingPlane = nearZ;
        projMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }
    
    void Camera::setFarClipPlane(const float farZ) {
        farClippingPlane = farZ;
        projMatrixNeedsUpdate = true;
        compositeMatrixNeedsUpdate = true;
    }

    const glm::mat4& Camera::getProjectionMatrix() const {
        if (projMatrixNeedsUpdate) {
            projMatrix = glm::perspective(
                glm::radians(fov),
                aspect,
                nearClippingPlane,
                farClippingPlane
            );

            projMatrixNeedsUpdate = false;
        }

        return projMatrix;
    }

    const glm::mat4& Camera::getViewMatrix() const {
        if (viewMatrixNeedsUpdate) {
            glm::mat4 origin = glm::translate(glm::mat4(1.f), m_origin);
            glm::mat4 origin_neg = glm::translate(glm::mat4(1.f), -m_origin);
            glm::mat4 trans = glm::translate(glm::mat4(1.f), -m_position);
            glm::mat4 rot = glm::lookAt(m_origin, m_origin + m_dir, glm::vec3(0.f, 1.f, 0.f));
            //viewMatrix = origin_neg * trans * rot * origin;
            viewMatrix = origin_neg * rot * trans * origin;

            viewMatrixNeedsUpdate = false;
        }

        return viewMatrix;
    }

    const glm::mat4& Camera::getViewProjMatrix() const {
        if (compositeMatrixNeedsUpdate) {
            composite = getProjectionMatrix() * getViewMatrix();

            compositeMatrixNeedsUpdate = false;
        }

        return composite;
    }

    glm::vec3 Camera::projectPoint(const glm::vec3 point) const {
        return getViewProjMatrix() * glm::vec4(point, 1.f);
    }
}