#include "Mesh.hpp"

namespace sgl {
    Mesh::Mesh(
        const std::vector<glm::vec3>& vertices,
        const std::vector<glm::vec3>& normals,
        const std::vector<Color>& colors,
        const std::vector<unsigned>& indices)
    : vertices(vertices),
      normals(normals),
      colors(colors),
      indices(indices) {;}

    Mesh Mesh::Plane(const math::Rect<float>& rect) {
        auto corners2d = rect.cornerVectors();
        std::vector<glm::vec3> corners3d = {
            glm::vec3(corners2d[0], 0.f),
            glm::vec3(corners2d[1], 0.f),
            glm::vec3(corners2d[2], 0.f),
            glm::vec3(corners2d[3], 0.f)
        };

        std::vector<glm::vec3> normals = {
            glm::vec3(0.f, 0.f, 1.f),
            glm::vec3(0.f, 0.f, 1.f)
        };

        std::vector<Color> colors = {
            Color(1.f, 1.f, 1.f, 1.f),
            Color(1.f, 1.f, 1.f, 1.f),
            Color(1.f, 1.f, 1.f, 1.f),
            Color(1.f, 1.f, 1.f, 1.f)
        };
        
        std::vector<unsigned> indices = {
            0, 1, 2,
            2, 3, 0
        };

        return Mesh(corners3d, normals, colors, indices);
    }

    Mesh Mesh::UnitBox() {
        std::vector<glm::vec3> vertices = {
            glm::vec3(0.f, 0.f, 0.f),
            glm::vec3(0.f, 0.f, 1.f),
            glm::vec3(1.f, 0.f, 1.f),
            glm::vec3(1.f, 0.f, 0.f),
            glm::vec3(0.f, 1.f, 0.f),
            glm::vec3(0.f, 1.f, 1.f),
            glm::vec3(1.f, 1.f, 1.f),
            glm::vec3(1.f, 1.f, 0.f),
        };

        std::vector<Color> colors = {
            glm::vec4(0.9f, 0.9f, 0.9f, 1.f),
            glm::vec4(0.9f, 0.15f, 0.9f, 1.f),
            glm::vec4(0.5f, 0.9f, 0.9f, 1.f),
            glm::vec4(0.15f, 0.9f, 0.9f, 1.f),
            glm::vec4(0.5f, 0.9f, 0.5f, 1.f),
            glm::vec4(0.15f, 0.9f, 0.3f, 1.f),
            glm::vec4(0.9f, 0.5f, 0.5f, 1.f),
            glm::vec4(0.9f, 0.15f, 0.3f, 1.f)
        };

        std::vector<glm::vec3> normals = {
            glm::vec3(0.f, 1.f, 0.f),
            glm::vec3(0.f, 1.f, 0.f),
            glm::vec3(0.f, -1.f, 0.f),
            glm::vec3(0.f, -1.f, 0.f),
            glm::vec3(0.f, 0.f, 1.f),
            glm::vec3(0.f, 0.f, 1.f),
            glm::vec3(1.f, 0.f, 0.f),
            glm::vec3(1.f, 0.f, 0.f),
            glm::vec3(0.f, 0.f, -1.f),
            glm::vec3(0.f, 0.f, -1.f),
            glm::vec3(-1.f, 0.f, 0.f),
            glm::vec3(-1.f, 0.f, 0.f)
        };

        std::vector<unsigned> indices = {
            0, 1, 2, 2, 3, 0,
            4, 7, 6, 6, 5, 4,
            1, 5, 6, 6, 2, 1,
            2, 6, 7, 7, 3, 2,
            3, 7, 4, 4, 0, 3,
            0, 4, 5, 5, 1, 0
        };

        return Mesh(vertices, normals, colors, indices);
    }
}