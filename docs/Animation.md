# Skeletal Animation
* Each vertex in an animated mesh stores a parent bone index and a weight, and will be affected by the transform of this bone.
* Each bone transform is constructed from its parent transform and the configuration of the current bone.
* The transforms are passed to the vertex shader as uniforms.
* The transforms are calculated on the CPU side.
* The skeleton can be seen as an acyclic tree of "bones", each of which stores the following information:
  * Offset from mesh origin, to allow rotation around the bone "joint"
* The bones do not need to know about their "child bones", so it can be efficiently computed "upwards",
by first transforming the root bone, and - in a breadth first manner - transform the child bones according to their parent bone transforms.
* This means that the transformation of a vertex is the compound transform of all ancestor bone transforms.
* For a mesh with N_v vertices and N_b bones, we have:
  * For precomputing the bone transforms:
    * N_b slerps
    * N_b generate 4x4 matrix from quaternion
    * 3 * N_b matrix 4x4 multiplications
  * Each transform is 16 * sizeof(float) bytes long
    * Copy of 16 * sizeof(float) * N_b bytes of transform data¨
  * 
```

```