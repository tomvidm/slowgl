# SlowGL
SlowGL should encapsulate rendering and event handling in a class, and provide an interface that allows use of this class for easy development of interactive graphics.

## Using SlowGL
Initialize a SlowGL object with a SlowGLConfig object.

## Rendering
SlowGL should support both 2D and 3D rendering.

* Mesh2D
  * A connected 2D mesh of vertices
* Mesh3D
  * A connected 3D mesh of vertices
* PrimitiveArray
  * An array of primitives (Triangles, Quads, Line segments) 


* Static Geometry
  * World space transforms performed before any rendering. No updates needed.
  * Identity model transform passed to vertex shader.
* Dynamic Geometry
  * For each mesh, set a uniform transform


On rendering:
* Build a draw list based on the following
  * A camera, to eliminate objects that are guaranteed not to fall inside view frustrum
* Set the opengl states needed
  * Set the view-projection matrix given by the Camera object
  * Other states?
* For each renderables:
  * Obtain the transformation matrix of the renderable R.
  * Construct the model-view-projection matrix and set the glUniform variable