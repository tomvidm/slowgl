#version 330 core

in vec4 vertexColor;
in vec3 worldSpacePosition;
in vec3 worldSpaceNormal;

out vec4 fragColor;

uniform mat3 normalMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;

uniform vec3 ambientLight;

uniform int numLights;
uniform vec4 lightIntensities[32];
uniform vec3 lightPositions[32];

vec4 diffuseDelta(int lightID) {
    vec3 toLight = worldSpacePosition - lightPositions[lightID];
    float attenuation = 1.0 / (dot(toLight, toLight));
    float diffuseIntensity = max(-dot(toLight, worldSpaceNormal), 0.0);

    return vertexColor * lightIntensities[lightID] * attenuation * diffuseIntensity;
}

void main() {
    for (int i = 0; i < int(numLights); ++i) {
        fragColor += diffuseDelta(i);
    }
}