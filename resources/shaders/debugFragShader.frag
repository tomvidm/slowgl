#version 330 core

in vec4 vertexColor;
in vec3 worldSpacePosition;
in vec3 worldSpaceNormal;

out vec4 fragColor;

uniform mat3 normalMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;

uniform vec3 ambientLight;

uniform int numLights;
uniform vec4 lightIntensities[32];
uniform vec3 lightPositions[32];

void main() {
    fragColor = vertexColor;
}