#version 330 core
layout (location = 0) in vec3 localPosition;
layout (location = 1) in vec3 localNormal;
layout (location = 2) in vec4 color;

out vec4 vertexColor;
out vec3 worldSpacePosition;
out vec3 worldSpaceNormal;

uniform mat3 normalMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform mat4 viewProjMatrix;

void main() {
    gl_Position = viewProjMatrix * modelMatrix * vec4(localPosition, 1.0);

    // The the screen position
    worldSpacePosition = vec3(modelMatrix * vec4(localPosition, 1.0));
    worldSpaceNormal = normalMatrix * localNormal;

    // Set the vertex color
    vertexColor = color;
}
